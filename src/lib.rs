use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::str::FromStr;

const NMAX: usize = 16;

pub struct Stack {
    values: Vec<u32>,
    guessed: Vec<u32>,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ParseError {
    IncorrectNumber(usize),
    InvalidCellValue,
}

impl ParseError {
    pub fn as_str(&self) -> &'static str {
        match *self {
            ParseError::IncorrectNumber(_n) => "Incorrect number of input values",
            ParseError::InvalidCellValue => "invalid cell -- value",
        }
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl Error for ParseError {
    fn description(&self) -> &str {
        self.as_str()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct Board {
    size: usize,
    values: Vec<u32>,
    choices: Vec<u32>,
    nchoice_coords: HashMap<u32, Vec<(usize, usize)>>,
    guessed: Vec<u32>,
}

fn isqrt(x: usize) -> usize {
    (x as f64).sqrt() as usize
}

fn is_square(x: usize) -> bool {
    let y = isqrt(x);
    y * y == x
}

impl Board {
    pub fn new(values: Vec<u32>) -> Board {
        let len = values.len();
        let size = (len as f64).sqrt() as usize;
        if size * size != len {
            panic!("Board array length is not square")
        }
        Board {
            size,
            values,
            choices: vec![0; len],
            nchoice_coords: HashMap::new(),
            guessed: vec![0; len],
        }
    }

    pub fn new_from_path(path: &Path) -> Result<Board, Box<dyn Error>> {
        let file = File::open(path)?;
        let mut buf_reader = BufReader::new(file);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents)?;
        let board = Board::from_str(&contents)?;
        Ok(board)
    }

    pub fn getrow(&self, i: usize) -> Vec<u32> {
        let start = i * self.size;
        self.values[start..start + self.size].to_vec()
    }

    pub fn getcol(&self, i: usize) -> Vec<u32> {
        let mut values: Vec<u32> = Vec::new();
        for j in (i..self.size * self.size).step_by(self.size) {
            values.push(self.values[j]);
        }
        values
    }

    pub fn getblock(&self, i: usize) -> Vec<u32> {
        let mut values: Vec<u32> = Vec::new();
        let blocksize = isqrt(self.size);
        let mut j = (i % blocksize) + (i / blocksize) * self.size;
        j *= blocksize;
        let mut k = 0usize;
        loop {
            values.push(self.values[j]);
            if values.len() == self.size {
                break;
            }
            k += 1;
            j += if k == blocksize {
                k = 0;
                self.size - blocksize + 1
            } else {
                1
            }
        }
        values
    }

    pub fn getblock_by_rowcol(&self, row: usize, col: usize) -> Vec<u32> {
        let blocksize = isqrt(self.size);
        self.getblock(blocksize * (row / blocksize) + col / blocksize)
    }

    pub fn is_valid(&self) -> bool {
        for i in 0..self.size {
            let values = self
                .getblock(i)
                .into_iter()
                .filter(|&i| i != 0)
                .collect::<Vec<_>>();
            let mut other = values.to_vec();
            other.sort();
            other.dedup();
            if values.len() != other.len() {
                return false;
            }
        }

        for i in 0..self.size {
            let values = self
                .getrow(i)
                .into_iter()
                .filter(|&i| i != 0)
                .collect::<Vec<_>>();
            let mut other = values.to_vec();
            other.sort();
            other.dedup();
            if values.len() != other.len() {
                return false;
            }
        }

        for i in 0..self.size {
            let values = self
                .getcol(i)
                .into_iter()
                .filter(|&i| i != 0)
                .collect::<Vec<_>>();
            let mut other = values.to_vec();
            other.sort();
            other.dedup();
            if values.len() != other.len() {
                return false;
            }
        }

        true
    }

    pub fn set_valid_choices(&mut self) {
        self.nchoice_coords.clear();
        let mask = (1 << (self.size + 1)) - 1;
        for row in 0..self.size {
            for col in 0..self.size {
                let i = row * self.size + col;
                let choice = if self.values[i] > 0 {
                    0
                } else {
                    let invalid = self
                        .getrow(row)
                        .iter()
                        .fold(0, |sum, value| sum | (1 << value))
                        | self
                            .getcol(col)
                            .iter()
                            .fold(0, |sum, value| sum | (1 << value))
                        | self
                            .getblock_by_rowcol(row, col)
                            .iter()
                            .fold(0, |sum, value| sum | (1 << value));
                    let invalid = invalid | self.guessed[i];
                    invalid ^ mask
                };
                self.choices[i] = choice;
                let len = choice.count_ones();
                if let Some(coords) = self.nchoice_coords.get_mut(&len) {
                    coords.push((row, col));
                //self.nchoice_coords.insert(choice.count_ones(), coords);
                } else {
                    self.nchoice_coords.insert(len, vec![(row, col)]);
                }
            }
        }
    }

    pub fn place(&mut self) -> usize {
        let mut nplaced = 0usize;
        for row in 0..self.size {
            for col in 0..self.size {
                let i = row * self.size + col;
                if self.choices[i].count_ones() == 1 {
                    self.values[i] = self.choices[i].trailing_zeros();
                    nplaced += 1;
                }
            }
        }
        nplaced
    }

    pub fn guess(&mut self, stack: &mut Vec<Stack>) -> bool {
        for k in 2..10 {
            if let Some(coords) = self.nchoice_coords.get_mut(&k) {
                if let Some((row, col)) = coords.pop() {
                    let i = row * self.size + col;
                    let value = self.choices[i].trailing_zeros();
                    self.guessed[i] |= 1 << value;
                    stack.push(Stack {
                        values: self.values.to_vec(),
                        guessed: self.guessed.to_vec(),
                    });
                    // stack.append((deepcopy(matrix), deepcopy(tested)))
                    self.values[i] = value;
                    return true;
                }
            }
        }
        false
    }

    pub fn solve(&mut self) -> (usize, usize) {
        let mut stack: Vec<Stack> = Vec::new();
        let mut n = 0usize;
        let mut nguess = 0;
        loop {
            n += 1;
            if !self.is_valid() {
                if let Some(item) = stack.pop() {
                    self.values = item.values;
                    self.guessed = item.guessed;
                } else {
                    panic!("sudoku is not solvable");
                }
            }
            self.set_valid_choices();

            let nplaced = self.place();
            if nplaced == 0 {
                if self.values.iter().filter(|&x| x == &0).count() == 0 {
                    break; // solved!
                }

                if self.guess(&mut stack) {
                    nguess += 1;
                } else if let Some(item) = stack.pop() {
                    self.values = item.values;
                    self.guessed = item.guessed;
                } else {
                    panic!("sudoku is not solvable");
                }
            }
        }
        (n, nguess)
    }
}

impl FromStr for Board {
    type Err = ParseError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let mut values: Vec<u32> = Vec::with_capacity(NMAX * NMAX);
        let mut nchars = 0usize;
        for (nline, line) in string.lines().enumerate() {
            let lline = line.trim();
            if lline.is_empty() {
                continue;
            }
            let blocks = lline.split_whitespace().collect::<Vec<&str>>();
            if blocks.len() == 1 {
                let nchars2 = blocks[0].len();
                if nchars == 0 {
                    nchars = nchars2;
                }
                if !is_square(nchars2) || nchars2 != nchars {
                    return Err(ParseError::IncorrectNumber(nline));
                }

                let line_values: Result<Vec<u32>, _> = blocks[0]
                    .chars()
                    .map(|c| match c {
                        '.' | 'o' | 'O' | 'x' | 'X' | '0' => Ok(0u32),
                        _ => c.to_digit(16).ok_or(ParseError::InvalidCellValue),
                    })
                    .collect();
                values.append(&mut line_values?);
            } else if is_square(blocks.len()) {
                let nchars2 = blocks.len();
                if nchars == 0 {
                    nchars = nchars2;
                }
                if nchars2 != nchars {
                    return Err(ParseError::IncorrectNumber(nline));
                }
                let line_values: Result<Vec<u32>, _> = blocks
                    .iter()
                    .map(|s| {
                        if let Some(c) = s.chars().next() {
                            match c {
                                '.' | 'o' | 'O' | 'x' | 'X' | '0' => Ok(0u32),
                                _ => c.to_digit(16).ok_or(ParseError::InvalidCellValue),
                            }
                        } else {
                            Err(ParseError::IncorrectNumber(nline))
                        }
                    })
                    .collect();
                values.append(&mut line_values?);
            } else {
                return Err(ParseError::IncorrectNumber(nline));
            }
        }
        values.shrink_to_fit();
        Ok(Board::new(values))
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut string = String::new();
        let rem = self.size - 1;
        let size_vert = isqrt(self.size) * self.size;
        let rem_vert = size_vert - 1;
        for (i, value) in self.values.iter().enumerate() {
            string.push_str(value.to_string().as_str());
            if i % self.size == rem {
                string.push('\n');
            } else {
                string.push(' ');
            }
            if i % size_vert == rem_vert {
                string.push('\n');
            }
        }
        write!(f, "{}", string)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn simple_board() {
        let values = vec![1, 2, 3, 4];
        let board = Board {
            size: 2,
            values: vec![1, 2, 3, 4],
            choices: vec![0; 4],
            nchoice_coords: HashMap::new(),
            guessed: vec![0; 4],
        };
        assert_eq!(Board::new(values), board);
    }

    #[test]
    #[should_panic(expected = "Board array length is not square")]
    fn bad_board_size() {
        let values = vec![1, 2, 3];
        let _board = Board::new(values);
    }

    #[test]
    fn board_from_string() {
        let text = "\
		    12.456789
            456.89123
            7891.3456
            23456.891
            567891.34
            0912345.7
            3o567891O
            67X912345
            9 1 2 . 4 5 6 7 8
            ";
        let board = Board::from_str(text).unwrap();
        assert_eq!(vec![1, 2, 0, 4, 5, 6, 7, 8, 9], board.getrow(0));
        assert_eq!(vec![7, 8, 9, 1, 0, 3, 4, 5, 6], board.getrow(2));
        assert_eq!(vec![9, 1, 2, 0, 4, 5, 6, 7, 8], board.getrow(8));
        assert_eq!(vec![1, 4, 7, 2, 5, 0, 3, 6, 9], board.getcol(0));
        assert_eq!(vec![0, 6, 9, 4, 7, 1, 5, 0, 2], board.getcol(2));
        assert_eq!(vec![9, 3, 6, 1, 4, 7, 0, 5, 8], board.getcol(8));
        assert_eq!(vec![1, 2, 0, 4, 5, 6, 7, 8, 9], board.getblock(0));
        assert_eq!(vec![7, 8, 9, 1, 2, 3, 4, 5, 6], board.getblock(2));
        assert_eq!(vec![5, 6, 0, 8, 9, 1, 2, 3, 4], board.getblock(4));
        assert_eq!(vec![9, 1, 0, 3, 4, 5, 6, 7, 8], board.getblock(8));
    }

    #[test]
    fn parse_from_string() {
        let _board: Board = "\
		    12.456789
            456.89123
            7891.3456
            23456.891
            567891.34
            0912345.7
            3o567891O
            67X912345
            9 1 2 . 4 5 6 7 8
            "
        .parse()
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "Board array length is not square")]
    fn board_invalid_size() {
        let text = "\
		    12.456789
            456.89123
            7891.3456
            23456.891
            567891.34
            0912345.7
            3o567891O
            67X912345
            ";
        Board::from_str(text).unwrap();
    }

    #[test]
    #[should_panic(expected = "called `Result::unwrap()` on an `Err` value: InvalidCellValue")]
    fn board_invalid_cell() {
        let text = "\
		    12.456789
            456.89123
            7891.3456
            23456.891
            567891.34
            0912345.7
            3o567891O
            67X912345
            9 1 2 Z 4 5 6 7 8
            ";
        Board::from_str(text).unwrap();
    }

    #[test]
    fn solve1() {
        let mut board = Board::from_str(
            "\
            504079006
            000430050
            307000280
            048607005
            000180400
            750000061
            085001093
            030060020
            009250100
            ",
        )
        .unwrap();
        let (niter, nguess) = board.solve();
        assert_eq!(6, niter);
        assert_eq!(0, nguess);
    }

    #[test]
    fn solve2() {
        let mut board = Board::from_str(
            "\
            000520000
            009000704
            200006100
            000008000
            080109060
            300050000
            051000008
            008000370
            002080001
            ",
        )
        .unwrap();
        let (niter, nguess) = board.solve();
        assert_eq!(143, niter);
        assert_eq!(24, nguess);
    }

    #[test]
    fn solve3() {
        let mut board = Board::from_str(
            "\
            020000000
            000600003
            074080000
            000003002
            080040010
            600500000
            000010780
            500009000
            000000040
            ",
        )
        .unwrap();
        let (niter, nguess) = board.solve();
        assert_eq!(3889, niter);
        assert_eq!(603, nguess);
    }

    #[test]
    fn solve4() {
        let mut board = Board::from_str(
            "\
            020000000
            000600003
            074080000
            000003002
            080040010
            600500000
            000010780
            500009000
            000000040
            ",
        )
        .unwrap();
        let (niter, nguess) = board.solve();
        assert_eq!(3889, niter);
        assert_eq!(603, nguess);
    }

    #[test]
    fn solve5() {
        let mut board = Board::from_str(
            "\
            000000000
            000000000
            000000000
            384000000
            000000000
            000000000
            000000000
            000000000
            000000002
            ",
        )
        .unwrap();
        let (niter, nguess) = board.solve();
        assert_eq!(74, niter);
        assert_eq!(45, nguess);
    }

    #[test]
    #[should_panic(expected = "sudoku is not solvable")]
    fn solve6() {
        let mut board = Board::from_str(
            "\
            5.4.79..6
            .2.43..5.
            3.7...28.

            .486.7..5
            ...18.4..
            75.....61

            .85..1.93
            .3..6..2.
            ..925.1..
            ",
        )
        .unwrap();
        board.solve();
    }
}

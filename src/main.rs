use std::env;
use std::path::Path;
use sudoku::Board;

fn main() {
    /*

        let text = "\
    12.456789
    456.89123
    7891.3456
    23456.891
    567891.34
    .912345.7
    3.567891.
    67.912345
    912.45678
    ";
        let _board = Board::new_from_string(text);

        let text = "\
    1 2 . 4 5 6 7 8 9
    4 5 6 . 8 9 1 2 3
    7 8 9 1 . 3 4 5 6
    2 3 4 5 6 . 8 9 1
    5 6 7 8 9 1 . 3 4
    . 9 1 2 3 4 5 . 7
    3 . 5 6 7 8 9 1 .
    6 7 . 9 1 2 3 4 5
    9 1 2 . 4 5 6 7 8
    ";
    let mut board = Board::new_from_string(text);
    println!("{}", board);
    println!();

    println!("row 0: {:?}", board.getrow(0));
    println!("row 2: {:?}", board.getrow(2));
    println!("row 8: {:?}", board.getrow(8));

    println!("col 0: {:?}", board.getcol(0));
    println!("col 2: {:?}", board.getcol(2));
    println!("col 8: {:?}", board.getcol(8));

    println!("block 0: {:?}", board.getblock(0));
    println!("block 2: {:?}", board.getblock(2));
    println!("block 4: {:?}", board.getblock(4));
    println!("block 8: {:?}", board.getblock(8));
    board.set_valid_choices();

    board.solve();
    println!("{}", board);
    println!("\n======================================\n");
     */

    let args: Vec<String> = env::args().collect();
    if args.len() > 1 {
        let mut board = Board::new_from_path(Path::new(&args[1])).unwrap();
        println!("Input:");
        println!("{}", board);

        let (niter, nguess) = board.solve();
        println!("Iterations = {}, guesses = {}", niter, nguess);
        println!();
        println!("Solution:");
        println!("{}", board);
    }
}
